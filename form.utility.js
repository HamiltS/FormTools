﻿; (function ($) {
    // jQuery function to add event handler to format numbers after change
    $.fn.formatNumber = function (decimals) {
        this.each(function (i) {
            $(this).change(function (e) {
                var value = parseFloat(this.value.replace(/,/g, ''));
                if (isNaN(value)) return;
                this.value = typeof decimals === 'number' ? value.toFixed(decimals) : value;
            });
        });
        return this; //for chaining
    }
}(jQuery));
