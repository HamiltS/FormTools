; (function ($) {

    /**
     * jQuery function to walk through forms, tables and input fields
     * with events to fire.  Used by getForm, setForm and lockForm.
     * Each event is passed varibles in an object bound to 'this'.
     * Returning true will continue the current loop.
     * Returning false will break the current loop.
     *
     * List of function hooks and the variables available:
     * PreForm - form
     *     PreTable - form, table, rows
     *         PreRow - form, table, rows, row, index
     *             Field - form, table, rows, row, index, fields, field
     *         PostRow - form, table, rows, row, index, fields
     *     PostTable - form, table, rows
     *     PreFields - form, fields
     *         Field - form, fields, field
     *     PostFields - form, fields
     * PostForm - form
     */
    $.fn.walkForms = function (funcs, options, vars) {
        var result;
        vars = $.extend({}, vars, {table: [], rows: [], row: [], index: [], fields: []});
        options = $.extend({}, options);
        if (typeof options !== 'object') {
            throw new Error("Invalid options object");
        }
        if (options.rowSelector === undefined) options.rowSelector = 'tbody tr';
        $(this).find('form').addBack('form').each(function () {
            // pre form
            vars.form = this;
            if ((result = funcs.PreForm && funcs.PreForm.call(vars, options)) !== undefined) return result;

            $(this).walkInputs(funcs, options, vars, result);

            if ((result = funcs.PostForm && funcs.PostForm.call(vars, options)) !== undefined) return result;
        });
        return this;
    }

    $.fn.walkInputs = function (funcs, options, vars, result) {
        $(this).each(function () {
            var top = this;
            $(top).find(options.tableSelector)
                    // remove tables inside tables, so only first 'layer' is selected
                    .filter(function () { return !$(this).parent().closest(options.tableSelector).closest(top).length; })
                    .each(function () {
                if (!this.id) return;  // skip tables without id
                vars.table.unshift(this);
                vars.rows.unshift($(this).find(options.rowSelector)
                    // remove rows inside tables
                    .filter(function () { return !$(this).parent().closest(options.rowSelector).closest(vars.table[0]).length; }));
                if ((result = funcs.PreTable && funcs.PreTable.call(vars, options)) !== undefined) {
                    vars.table.shift();
                    vars.rows.shift();
                    return result
                };
                // PreTable may change the row count, look for rows again
                vars.rows[0] = $(this).find(options.rowSelector)
                    // remove rows inside tables
                    .filter(function () { return !$(this).parent().closest(options.rowSelector).closest(vars.table[0]).length; });
                vars.rows[0].each(function (index) {
                    vars.index.unshift(index);
                    vars.row.unshift(this);
                    if ((result = funcs.PreRow && funcs.PreRow.call(vars, options)) !== undefined) {
                        vars.index.shift();
                        vars.row.shift();
                        return result;
                    }
                    // recursive to look for tables in tables
                    $(this).walkInputs(funcs, options, vars, result);
                    if ((result = funcs.PostRow && funcs.PostRow.call(vars, options)) !== undefined) {
                        vars.index.shift();
                        vars.row.shift();
                        vars.fields.shift();
                        return result;
                    }
                    vars.fields.shift();
                    vars.row.shift();
                    vars.index.shift();
                });
                if ((result = funcs.PostTable && funcs.PostTable.call(vars, options)) !== undefined) {
                    vars.table.shift();
                    vars.rows.shift();
                    return result;
                }
                vars.rows.shift();
                vars.table.shift();
            });
            vars.fields.unshift($(this).find(options.tags.join(', ')));
            if (options.tableSelector) vars.fields[0] = vars.fields[0]
                // remove fields in rows in tables that were selected above
                .filter(function () {
                    return !$(this).parent().closest(options.rowSelector).closest(options.tableSelector).closest(top).length;
                })

            if ((result = funcs.PreFields && funcs.PreFields.call(vars, options)) !== undefined) return result;
            vars.fields[0].each(function () {
                vars.field = this;
                if ((result = funcs.Field && funcs.Field.call(vars, options)) !== undefined) return result;
                delete vars.field;
            });
            if ((result = funcs.PostFields && funcs.PostFields.call(vars, options)) !== undefined) return result;
            vars.fields.shift();
        });
    }

    /**
     * jQuery function to turn a list of input tags into an object.
     * -   does not skip any inputs regardless of value and type
     * -   multiple inputs with same name stored as array
     * -   select-multiple inputs stored as array
     * -   multiple radio buttons condense to 1 property
     * -   checkbox supports indeterminate values
     * Consider using getForm for form and table support
     * @returns {object} object with 1 property per input name
     * @example
     * // finds all input, textarea and select tags
     * var data = $('input, textarea, select').getInput()
     */
    $.fn.getInput = function (options) {
        var data = {};
        this.each(function () {
            var name = get_name(this)
                .replace(/\[.+\]$/, '');
            var keep;
            if (options.dirty !== undefined) keep = keep || (options.dirty === !!$(this).data('dirty'));
            if (options.hidden !== undefined) keep = keep || (options.hidden === (this.type === 'hidden'));
            if (options.required !== undefined) keep = keep || (options.required === !!$(this).prop('required'));
            if (typeof keep === 'boolean' && !keep) return;
            var value = this.value;
            if (typeof value === 'string') value = value.trim();
            if (this.tagName === 'OUTPUT') {
                value = $(this).text().trim();
            }
            var override = false;
            switch (this.type) {
                case "checkbox":
                    value = this.indeterminate ? undefined : this.checked;
                    break;
                case "radio":
                    if (data[name] === undefined) {
                        data[name] = "";          // ensure value for radio button even when nothing checked
                    }
                    if (!this.checked)
                        name = ""; // skip if not checked
                    else
                        override = true;          // prevent turning into array
                    break;
                case "select-multiple":
                    value = [];
                    $(this.options).each(function () {
                        if (this.selected) value.push(this.value.trim());
                    });
                    break;
            }
            if (name) {
                dataSet(data, name, value, override);
            }
        });
        return data;
    }

    /**
     * jQuery function to turn a list of forms into an object.
     * -   All input tags will be inside a form object named after the form name
     * Pass in a optional table and optional row selector to use array outputs for tables
     * -   All tables will be inside the form object in a table object named after the table id
     * -   All input tags inside of the table and tr selectors will be in an array
     * -   All input tags outside of the table selector will be in the form object
     * @param {object} options - object of optional settings
     *        {string} tableSelector - jQuery selector to specify tables to turn into arrays
     *        {string} rowSelector - jQuery selector to filter the table rows to get
     *        {boolean} dirty - include or exclude inputs and table rows with a data-dirty attribute
     *        {boolean} hidden - include or exclude inputs that are hidden
     *        {boolean} required - include or exclude inputs that are required
     * @returns {object} object containing 1 object per form
     * @example
     * // get all forms
     * var data = $('form').getForm()
     * // get specific form by name
     * var data = $('form[name="myform"]').getForm()
     * // get specific table by id in specific form by name
     * var data = $('form[name="myform"]').getForm({tableSelector: 'table#mytable'})
     * // get specific table by id in specific form by name, include specific rows only
     * var data = $('form[name="myform"]').getForm({tableSelector: 'table#mytable', rowSelector: 'tr[data-dirty="true"]'})
     */
    $.fn.getForm = function (options) {
        options = options || {};
        if (options.rowSelector === undefined) options.rowSelector = 'tbody tr';
        options.tags = options.tags || $.fn.getForm.tags;
        var result = { data: {} };
        this.walkForms($.fn.getForm.funcs, options, result);
        return result.data;
    }
    $.fn.getForm.funcs = {
        PreForm: function (options) {
            this.data[this.form.name] = {};
        },
        PreTable: function (options) {
            this.data[this.form.name][this.table[0].id] = [];
        },
        PreRow: function (options) {
            if (options.dirty !== undefined && options.dirty === !$(this.row[0]).data('dirty')) return true; // skip row
        },
        PostRow: function (options) {
            var rowdata = this.fields[0].getInput(options);
            if (!rowdata.id && $(this.row[0]).data('id')) rowdata.id = $(this.row[0]).data('id');
            this.data[this.form.name][this.table[0].id].push(rowdata);
        },
        PreFields: function (options) {
            if (!this.table[0]) {
                $.extend(this.data[this.form.name], this.fields[0].getInput(options));
            }
            return true; // skip fields
        }
    }
    $.fn.getForm.tags = ['input', 'select', 'textarea'];

    $.fn.isFormDirty = function (options) {
        options = options || {};
        if (options.rowSelector === undefined) options.rowSelector = 'tbody tr';
        options.tags = options.tags || $.fn.isFormDirty.tags;
        var result = { dirty: false };
        this.walkForms($.fn.isFormDirty.funcs, options, result);
        return result.dirty;
    }
    $.fn.isFormDirty.funcs = {
        PreTable: function (options) {
            if (!this.table[0].id) return;
        },
        PostTable: function (options) {
            if (this.dirty) return false; // skip rest of the tables
        },
        PreRow: function (options) {
            if ($(this.row[0]).data('dirty')) {
                this.dirty = true;
                return false; // skip rest of the rows
            }
        },
        Field: function (options) {
            if ($(this.field).data('dirty')) {
                this.dirty = true;
                return false; // skip rest of fields
            }
        },
        PostForm: function (options) {
            if (this.dirty) return true; // skip rest of the forms
        }
    }
    $.fn.isFormDirty.tags = ['input', 'select', 'textarea'];

    /**
     * jQuery function to fill a list of input tags from an object.
     * Consider using setForm for form and table support
     * @param {object} object containing properties that match the input names
     * @example
     * // fills all input, textarea and select tags
     * $('input, textarea, select').setInput(data)
     */
    $.fn.setInput = function (data, options) {
        $.fn.setInput.running = true;
        var multis = {};
        var self = this;
        this.each(function () {
            // allow for names to include . to dig into object hierachries
            var value = get_name(this)
                .replace(/\[.+\]$/, '')
                .split('.')
                .reduce(function (data, prop) {
                    return data ? data[prop] : data;
                }, data);
            if (value === undefined) return;
            // separate values for input names used more than once
            if (Array.isArray(value)) {
                if (this.type === 'select-one') {
                    if (self.find().addBack('select[name="' + this.name + '"]').length === 1) {
                        $(this).setSelect(value);
                        return;
                    }
                }
                if (this.type !== 'select-multiple') {
                    if (multis[this.name] !== undefined) {
                        multis[this.name] += 1;
                    } else {
                        multis[this.name] = 0;
                    }
                    value = value[multis[this.name]];
                }
            }
            if (this.type === 'checkbox') {
                if (typeof value !== 'boolean') {
                    this.indeterminate = true;
                    this.checked = true;
                } else {
                    this.checked = value;
                }
            } else if (this.type === 'radio') {
                if (typeof value === 'boolean') {
                    if (value === true) {
                        this.checked = (this.value.toLowerCase() === "true");
                    } else if (value === false) {
                        this.checked = (this.value.toLowerCase() === "false");
                    }
                } else {
                    // radio button may need value set from data
                    if (!$(this).attr('value')) {
                        $(this).attr('value', value);
                    } else {
                        this.checked = value === this.value;
                    }
                }
            } else if ($(this).hasClass('hasDatepicker')) {
                if (typeof value === 'string') {
                    var date = toDate(value);
                    if (!isNaN(date.getYear())) {
                        $(this).datepicker('setDate', date);
                    }
                }
            } else if (this.type === 'date') {
                if (typeof value === 'string') {
                    var date = toDate(value);
                    if (!isNaN(date.getYear())) {
                        $(this).val((new Date(date - date.getTimezoneOffset() * 60000)).toISOString().slice(0, 10));
                    }
                }
            } else if (this.tagName === 'OUTPUT') {
                if (value === null) return;
                if ($(this).attr('type') === 'date') {
                    var date = toDate(value);
                    if (!isNaN(date.getYear())) {
                        $(this).text((new Date(date - date.getTimezoneOffset() * 60000)).toISOString().slice(0, 10));
                    }
                } else if ($(this).attr('type') === 'datetime') {
                    var date = toDate(value);
                    if (!isNaN(date.getYear())) {
                        $(this).text((new Date(date - date.getTimezoneOffset() * 60000)).toISOString().slice(0, 10) + ' ' + date.toLocaleString('en-US', { hour12: true, hour: 'numeric', minute: 'numeric' }));
                    }
                } else {
                    $(this).text(value.toString());
                }
            } else {
                if (value === null) return;
                $(this).val(value.toString());
            }
            if (options.trigger) $(this).trigger('change');
            if (typeof options.setDirty === 'boolean') $(this).attr('data-dirty', options.setDirty);
        });
        if (options.dirty) {
            $(this)
                .filter(function () {
                    return $(this).data('dirty-event') !== true;
                })
                .change(function (evt) {
                    if (!$.fn.setInput.running) {
                        if (this.type === 'radio' || this.type === 'checkbox') {
                            $(this).attr('data-dirty', $(this).data('original') !== $(this).is(':checked'));
                        } else {
                            $(this).attr('data-dirty', $(this).data('original') !== $(this).val());
                        }
                    }
                })
                .data('dirty-event', true)
                .each(function (idx, el) {
                    if (el.type === 'radio' || this.type === 'checkbox') {
                        $(el).data('original', $(el).is(':checked'));
                    } else {
                        $(el).data('original', $(el).val())
                    }
                })
            ;
        }
        if (options.dirtyRow) {
            var dirtyRow = options.dirtyRow;
            $(this)
                .filter(function () {
                    return $(this).data('dirty-row-event') !== true;
                })
                .change(function (evt) {
                    if (!$.fn.setInput.running) {
                        $(dirtyRow).attr('data-dirty', true);
                    }
                })
                .data('dirty-row-event', true);
        }
        $.fn.setInput.running = false;
        return this;
    }

    $.fn.setSelect = function (options) {
        $(this).find('option[data-set=true]').remove();
        options.forEach(function (option) {
            this.append($('<option />')
                .attr('value', option.value || option)
                .text(option.text || option.value || option)
                .prop('selected', option.selected || false)
                .attr('data-set', true));
        }, this);
        return this;
    }

    /**
     * jQuery function to fill a list forms from an object.
     * -   Object properties must match form names
     * Pass in a optional table and optional row selector to use array outputs for tables
     * -   Table data must be property inside form property that matches table id
     * -   Table data must be an array of objects whos property names match the input names inside the tr tags.
     * @param {object} object containing properties that match the form names
     * @param {string} tableSelector - jQuery selector to specify tables to fill from arrays
     * @param {string} rowSelector - jQuery selector to filter the table rows to fill
     * @returns {object} object containing 1 object per form
     * @example
     * // set to all forms
     * $('form').setForm(data)
     * // set specific form by name
     * var data = {myform: {...}}
     * $('form[name="myform"]').setForm(data)
     * // set specific table by id in specific form by name
     * var data = {myform: {mytable: [{...}, {...}]}}
     * $('form[name="myform"]').setForm(data, 'table#mytable')
     */
    $.fn.setForm = function (data, options) {
        options = options || {};
        if (options.rowSelector === undefined) options.rowSelector = 'tbody tr';
        options.tags = options.tags || $.fn.setForm.tags;
        return this.walkForms($.fn.setForm.funcs, options, { data: data });
    }
    $.fn.setForm.funcs = {
        PreForm: function (options) {
            this.intable = 0;
            this.formdata = [this.data[this.form.name]];
            if (!this.formdata[0]) {
                console.log('setForm data did not include object for form "' + this.form.name + '"');
                console.log(this.data);
                return true; // skip form
            }
        },
        PreTable: function (options) {
            if (this.intable > 0) {
                this.formdata.unshift(this.formdata[0][this.index[0]][this.table[0].id]);
            } else {
                this.formdata.unshift(this.formdata[0][this.table[0].id]);
            }
            if (!this.formdata[0]) {
                console.log('setForm data did not include array for table "' + this.table[0].id + '"');
                this.formdata.shift();
                console.log(this.formdata[0]);
                return true; // skip table
            }
            var max = Math.max(this.rows[0].length, this.formdata[0].length);
            for (var i = 0; i < max; i++) {
                var row;

                if (i >= this.formdata[0].length) {
                    // remove extra rows
                    if (i > 0) {
                        $(this.rows[0][i]).remove();
                    } else {
                        $(this.rows[0][i]).hide();
                    }
                    continue;
                } else if (i < this.rows[0].length) {
                    row = $(this.rows[0][i]);
                    // ensure visible
                    row.show();
                } else {
                    // duplicate first row of table for each extra row
                    row = this.rows[0].first().clone(true, true);
                    row.appendTo(this.rows[0].parent());
                }
                // corrections for duplicated ids, radio buttons and datepickers
                row.find(options.tags.join(', ')).removeAttr('id');
                var rowid = this.table[0].id + '.' + i;
                // find radio inputs without value to mark them permanently as not needing name mangling
                row.find('input[type=radio]:not([value])')
                    .attr('data-no-mangle', true);
                row.find('input[type=radio]:not([data-no-mangle])')
                    .attr('name', function (index, value) {
                        return value.replace(/\[.+\]$/, '') + '[' + rowid + ']';
                    });
                if ($.datepicker) {
                    row.find('.hasDatepicker')
                        .removeClass('hasDatepicker')
                        .removeData('datepicker')
                        .unbind()
                        .datepicker();
                }
            }
        },
        PostTable: function (options) {
            this.formdata.shift();
        },
        PreRow: function (options) {
            this.intable += 1;
            if (!this.formdata[0][this.index[0]]) {
                console.log('setForm table data does not include row ' + this.index[0]);
                this.intable -= 1;
                return true;
            }
        },
        PostRow: function (options) {
            this.intable -= 1;
        },
        PreFields: function (options) {
            if (this.intable <= 0) {
                this.fields[0].setInput(this.data[this.form.name], options);
            } else {
                if (options.dirty) options.dirtyRow = this.row[0];
                this.fields[0].setInput(this.formdata[0][this.index[0]], options);
            }
        }
    }
    $.fn.setForm.tags = ['input', 'select', 'textarea', 'output'];

    $.fn.ensureVisible = function () {
        var panels = this.closest('.ui-tabs-panel');
        if (panels.length > 0) {
            var id = [0].id;
            this.closest('.ui-tabs').tabs('option', 'active', this.closest('.ui-tabs-panel').index() - 1);
        }
    }

    $.fn.validateForm = function (options) {
        options = $.extend({}, options);
        var success = true;
        $(this).find('form').addBack('form').each(function () {
            if (!this.checkValidity() & !options.quiet) {
                var tempSubmit = $('<input type="submit" style="display: none;" />').appendTo($(this));
                $(this).ensureVisible();
                $(this).find(':submit').click();
                tempSubmit.remove();
                return success = false;
            }
        });
        return success;
    }

    /*
    rule object
    {
        Form: 'formname' || '*',
        Table: 'tableid' || '*',
        Row: 'rowid' || '*',
        Field: 'fieldname' || '*',
        Locked: true||false||null,
        Required: true||false||null
    }
    */
    $.fn.lockForm = function (rules, options) {
        options.tags = options.tags || ['input', 'select', 'textarea', 'output'];
        return this.walkForms($.fn.lockForm.funcs, options, { rules: rules });
    }
    $.fn.lockForm.funcs = {
        Field: function (options) {
            var rule = this.rules.find(function (rule) {
                // api call already filtered on queue
                var use_rule = (
                                                (!rule.Form || rule.Form === this.form.name || rule.Form === '*') &&
                                                ((this.table[0] === undefined && !rule.Table) ||
                                                 (this.table[0] !== undefined && (rule.Table === this.table[0].id || rule.Table === '*'))) &&
                                                ((this.rowid === undefined && !rule.Row) ||
                                                 (this.rowid !== undefined && (rule.Row === this.row[0].data('id') || rule.Row === '*'))) &&
                                                (!rule.Field || rule.Field === get_name(this.field) || rule.Field === '*') &&
                                                (!rule.State || rule.State in options.state || rule.State === '*')
                );
                return use_rule;
            }, this);
            if (rule) {
                switch (rule.Flag) {
                    case 'locked':
                        this.field.disabled = true;
                        this.field.required = false;
                        break;
                    case 'unlocked':
                        this.field.disabled = false;
                        this.field.required = false;
                        break;
                    case 'required':
                        this.field.disabled = false;
                        this.field.required = true;
                        break;
                    default:
                        this.field.disabled = true;
                        this.field.required = false;
                        break;
                }
            }
        }
    }

    function toDate(value) {
        if (value instanceof Date) {
            return value;
        } else if (typeof value === 'string' && value.startsWith('/Date(')) {
            return new Date(parseInt(value.substr(6)));
        } else {
            return new Date(value);
        }
    }

    $.toDate = toDate;

    function get_name(el) {
        var name = el.name;
        if (name === undefined) {
            var attr = el.attributes['name'];
            if (attr) {
                return attr.value;
            }
        } else {
            return name;
        }
        return "";
    }

    function dataGet(data, name) {
        return name.split('.').reduce(function (data, prop) {
            return data ? data[prop] : data;
        }, data);
    }

    function dataSet(data, name, value, override) {
        var dot = name.indexOf('.');
        if (dot >= 0) {
            var part = name.slice(0, dot);
            if (data[part] === undefined) {
                data[part] = {};
            }
            dataSet(data[part], name.slice(dot + 1), value, override);
        } else {
            if (!override && data[name] !== undefined) {
                if (!data[name].push) {
                    data[name] = [data[name]];
                }
                data[name].push(value);
            } else {
                data[name] = value;
            }
        }
    }

    // this is probably not needed, json and mssql do the right thing nowadays
    function Asciify(str) {
        Asciify.replacements.forEach(function (replacement) {
            replacement[1].forEach(function (badchar) {
                str = str.replace(badchar, replacement[0]);
            })
        })
        return str.replace(/[^\x00-\x7f]/g, '');
    }
    Asciify.replacements = [
        ['<<', ['\u00ab']],
        ['>>', ['\u00bb']],
        ["'", ['\u2018', '\u2019', '\u201a', '\u201b']],
        ['"', ['\u201c', 'u201d', '\u201e', '\u201f']],
        ['<', ['\u2039']],
        ['>', ['\u203a']],
        ['-', ['\u2013', '\u2014']],
        ['...', ['\u2026']]
    ]

}(jQuery));
