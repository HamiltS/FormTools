﻿; (function (window) {

    var Loading = {};

    Loading.MainStyle = {
        position: 'fixed',
        right: '0px',
        top: '0px',
        bottom: '0px',
        left: '0px',
        'background-color': 'rgba(248, 248, 248, 0.825)',
        'z-index': '30001',
        display: 'flex',
        'align-items': 'center',
        'justify-content': 'center',
        'text-align': 'center'
    };

    Loading.TextStyle = {
        color: 'darkgrey',
        'font-size': '2rem',
    };

    Loading.ButtonStyle = {
        'font-size': '1.25rem',
        'border-radius': '0.25rem',
        'border': 'solid 1px black',
        'min-width': '6rem'
    }

    Loading.ErrorPreStyle = {
        'font-family': 'Consolas',
        'font-size': '10pt',
        overflow: 'scroll',
        'max-height': '50%',
        'max-width': '75%',
        'text-align': 'left',
        margin: 'auto',
        'background-color': 'white'
    }

    Loading.Images = {
        wait: [
            '<span style="color: black">&#9692;</span>',
            '<span style="color: black">&#9696;</span>',
            '<span style="color: black">&#9693;</span>',
            '<span style="color: black">&#9694;</span>',
            '<span style="color: black">&#9697;</span>',
            '<span style="color: black">&#9695;</span>',
        ],
        success: '<span style="color: lightgreen;">&#10004;</span>',
        failure: '<span style="color: red;">&#10008;</span>'
    };

    Loading.fadeOutDelay = 500;
    Loading.fadeInDelay = 500;
    Loading.animateDelay = 83;

    Loading.animate = function () {
        if (Loading.active && typeof Loading.cycle === 'number') {
            Loading.cycle = (Loading.cycle + 1) % Loading.Images[Loading.animateImage].length;
            Loading.active.find('#image').html(Loading.Images[Loading.animateImage][Loading.cycle]);
            setTimeout(Loading.animate, Loading.animateDelay);
        }
    }

    Loading.Show = function (text, image, button, callback) {
        function getLoadingImage() {
            var loadingImage = Loading.Images[image];
            if (Array.isArray(loadingImage)) {
                Loading.cycle = 0;
                Loading.animateImage = image;
                loadingImage = loadingImage[0];
                setTimeout(Loading.animate, Loading.animateDelay);
            } else {
                Loading.cycle = undefined;
            }
            return loadingImage;
        }
        image = image || 'wait';
        if (!Loading.active) {
            Loading.active = $('<div></div>')
                .append(
                    $('<div><p><span id="image">' + getLoadingImage() + '</span>&nbsp;<label>' + text + '</label></p></div>')
                        .css(Loading.TextStyle)
                )
                .css(Loading.MainStyle)
                .hide()
                .appendTo('body')
                .fadeIn(Loading.fadeInDelay);
        } else {
            Loading.active.find("label").text(text);
            Loading.active.find("#image").html(getLoadingImage());
            Loading.active.find('pre').remove();
        }
        if (button) {
            Loading.active.find('input').remove();
            var btn = $('<input type="button" />')
                .val(button)
                .css(Loading.ButtonStyle)
                .click(function () {
                    Loading.Hide(0, callback);
                })
            Loading.active.find('div').append(btn);
        }
        return Loading.active;
    }

    Loading.Error = function (error, button, callback) {
        var text;
        var extra;
        if (typeof error === 'object') {
            if (error.ExceptionMessage) {
                text = error.ExceptionMessage;
                extra = error.StackTrace;
            } else if (error.Message) {
                text = error.Message
            }
        } else {
            text = error || "Error";
        }
        button = button || "Ok";
        Loading.Show(text, 'failure', button, callback);
        if (extra) {
            Loading.active.find('div').append($('<pre>' + extra + '</pre>').css(Loading.ErrorPreStyle));
        }
    }

    Loading.Hide = function (delay, callback) {
        if (Loading.active) {
            Loading.active
                .stop()
                .delay(delay)
                .fadeOut(Loading.fadeOutDelay, function () {
                    Loading.active.remove();
                    delete Loading.active;
                    if ($.isFunction(callback)) callback();
                });
        }
    }

    window.Loading = Loading;

}(window));
