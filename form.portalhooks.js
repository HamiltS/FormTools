﻿; (function (window) {

    var PortalHooks = {};

    PortalHooks.workItemLock = function () {

        if (typeof parent.top.updateWorkItemLockStatus == 'function') {
            parent.top.updateWorkItemLockStatus('lock');
        }
        else {
            console.log('Portal Framework not detected');
        }
    }

    PortalHooks.workItemUnlock = function () {

        if (typeof parent.top.updateWorkItemLockStatus == 'function') {
            parent.top.updateWorkItemLockStatus('unlock');
        }
        else {
            console.log('Portal Framework not detected');
        }
    }

    PortalHooks.workItemCompleted = function () {

        if (typeof parent.top.WorkItemCompleted == 'function') {
            parent.top.WorkItemCompleted();
        }
        else {
            window.open('', '_self', '');
            window.close();
            window.location.href = "about:blank";
        }
    }

    PortalHooks.workItemsRefresh = function () {
        if (typeof parent.top.UpdateWorkItemNeedsRefresh == 'function') {
            parent.top.UpdateWorkItemNeedsRefresh(formData.WorkItemID);
        }
    }
    
    PortalHooks.workItemClose = function () {

        if (typeof parent.top.WorkItemClose == 'function') {
            parent.top.WorkItemClose();
        }
        else {
            window.open('', '_self', '');
            window.close();
            window.location.href = "about:blank";
        }
    }

    window.PortalHooks = PortalHooks;

}(window));