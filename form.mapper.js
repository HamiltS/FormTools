; (function ($) {
    /**
     * Transforms one object into another using a definition object.
     * -   definition object should look like desired output object
     * -   definition object property names and values can use dot notation
     *     to access object properties and array indexes
     * -   supports arrays and sub-arrays
     * -   supports transformation functions with def property
     * @param {object} source object to transform
     * @param {object} definition object, specifies property names to map to source values
     * @param {boolean} invert the definition, used to transform the other way
     * @param {object} optional object to add values to
    
     * @example
     * // transform simple object to complex object
     * $.mapper({values: {one: 1, two: 2}}, {numbers: {first: 'values.one', second: 'values.two'}})
     * // returns {numbers: {first: 1, second: 2}}
     *
     * // identical transformation using dot notation
     * $.mapper({one: 1, two: 2}, {'numbers.first': 'values.one', 'numbers.second': 'values.two'})
     * // returns {numbers: {first: 1, second: 2}}
     *
     * // invert transformation using same definition
     * $.mapper({numbers: {first: 1, second: 2}}, {numbers: {first: 'values.one', second: 'values.two'}});
     * // returns {values: {one: 1, two: 2}}
     *
     * // transform object inside an array
     * $.mapper({things: [{name: 'thing 1'}, {name: 'thing 2'}]}, {list: [{value: 'things.[].name'}]})
     * // returns {list: [{value: 'thing 1'}, {value: 'thing 2'}]}
     */
    $.mapper = function (source, def, invert, dest) {
        var flat = $.mapper.flatten(def, invert);
        //console.log(flat);
        return flat
            .reduce(function (dest, destEntry) {
                (function extract(srcObj, destObj, srcDef, destParts, transforms, filters) {
                    var destPart = destParts[0];
                    if (destPart === '[]') {
                        var srcArr = get_value(srcObj, srcDef, filters, source);
                        if (srcArr) {
                            for (var i = 0; i < srcArr.length; i++) {
                                extract(srcArr, destObj,
                                    srcDef.indexOf('[]') >= 0 ? i + srcDef.substr(srcDef.indexOf('[]') + 2) : srcDef,
                                    [i].concat(destParts.slice(1)), transforms, filters ? filters.slice(1) : undefined);
                            }
                        } else {
                            console.log('mapper did not find source array ' + srcDef);
                        }
                    } else if (destPart === '{}') {
                        if (srcDef.indexOf('{}') >= 0) {
                            extract(get_value(srcObj, srcDef, transforms, source), destObj, srcDef.substr(srcDef.indexOf('{}') + 3),
                                destParts.slice(1), transforms ? transforms.slice(1) : undefined, filters);
                            return destObj['undefined'];
                        } else {
                            return get_value(srcObj, srcDef, transforms, source)
                        }
                    } else if (destParts.length > 1) {
                        if (!destObj[destPart]) {
                            destParts[1] === '[]' ? (destObj[destPart] = []) : (destObj[destPart] = {})
                        }
                        if (destParts[1] == '{}') {
                            destObj[destPart] = extract(srcObj, destObj[destPart], srcDef, destParts.slice(1), transforms, filters);
                        } else {
                            extract(srcObj, destObj[destPart], srcDef, destParts.slice(1), transforms, filters);
                        }
                    } else {
                        set_value(destObj, destPart, get_value(srcObj, srcDef, transforms, source));
                    }
                }(source, dest, destEntry.value, destEntry.key.split('.'),
                    destEntry.transforms ? destEntry.transforms.slice(0) : undefined,
                    destEntry.filters ? destEntry.filters.slice(0) : undefined));
                return dest;
            }, dest || {});
    }

    // flatten object hierarchy into dot notation list of key/value entries
    $.mapper.flatten = function (def, invert, prefix, transforms, filters) { // split entry to key and filter/transform array
        prefix = prefix || '';
        transforms = transforms || [];
        filters = filters || [];
        function dot(first, second) {
            if (first && second)
                return first + '.' + second;
            else
                return first + second;
        }
        function make_entry(prop, value) {
            var entry = {
                key: dot(prefix, prop),
                value: value
            }
            if (transforms.length) entry.transforms = transforms;
            if (filters.length) entry.filters = filters;
            if (entry.value === '') {
                entry.value = prop;
            }
            if (entry.value === '.') {
                entry.value = dot(prefix, prop);
            }
            if (invert) {
                var temp = entry.key;
                entry.key = entry.value;
                entry.value = temp;
                delete entry.transforms;
                delete entry.filters;
            }
            return entry;
        }
        if (typeof def === 'string') {
            return [make_entry('', def)];
        } else if (Array.isArray(def)) {
            return $.mapper.flatten(def[0], invert, dot(prefix, '[]'), transforms, filters);
        } else if (typeof def === 'function') {
            var func = def;
            //var newDef = {};
            //newDef[prop] = func.def || prop;
            if (func.type === 'filter') {
                //filters.push(entry.value);
                return [].concat($.mapper.flatten(func.def || prefix, invert, dot(prefix, '[]'), transforms, filters.concat(func)));
                //entry.value = entry.value.def || prop;
            } else {
                //transforms.push(entry.value);
                var stuff = [].concat($.mapper.flatten(func.def || prefix, invert, dot(prefix, '{}'), transforms.concat(func), filters));
                return stuff;
                //entry.value = entry.value.def || prop;
            }
        } else {
            return Object.keys(def).reduce(function (obj, prop) {
                if (Array.isArray(def[prop])) {
                    return obj.concat($.mapper.flatten(def[prop][0], invert, dot(dot(prefix, prop), '[]'), transforms, filters));
                } else if (typeof def[prop] === 'object') {
                    if (Object.keys(def[prop]).length === 0) {
                        var entry = {
                            key: dot(prefix, prop)
                        }
                        if (transforms.length) entry.transforms = transforms;
                        if (filters.length) entry.filters = filters;
                        obj.push(entry);
                        return obj;
                    } else {
                        return obj.concat($.mapper.flatten(def[prop], invert, dot(prefix, prop), transforms, filters));
                    }
                } else if (typeof def[prop] === 'function') {
                    var func = def[prop];
                    var newDef = {};
                    newDef[prop] = func.def || prop;
                    if (func.type === 'filter') {
                        //filters.push(entry.value);
                        return obj.concat($.mapper.flatten(newDef[prop], invert, dot(dot(prefix, prop), '[]'), transforms, filters.concat(func)));
                        //entry.value = entry.value.def || prop;
                    } else {
                        //transforms.push(entry.value);
                        return obj.concat($.mapper.flatten(newDef[prop], invert, dot(dot(prefix, prop), '{}'), transforms.concat(func), filters));
                        //entry.value = entry.value.def || prop;
                    }
                } else {
                    var entry = {
                        key: dot(prefix, prop),
                        value: def[prop]
                    }
                    if (entry.value === undefined) {
                        console.log('error');
                    }
                    if (transforms.length) entry.transforms = transforms;
                    if (filters.length) entry.filters = filters;
                    if (entry.value === '') {
                        entry.value = prop;
                    }
                    if (entry.value === '.') {
                        entry.value = dot(prefix, prop);
                    }
                    if (entry.value.slice(-1) === '.') {
                        entry.value = entry.value + prop;
                    }
                    if (invert) {
                        var temp = entry.key;
                        entry.key = entry.value;
                        entry.value = temp;
                        delete entry.transforms;
                        delete entry.filters;
                    }
                    return obj.concat(entry);
                }
            }, []);
        }
    }

    // set value in new object intelligently
    function set_value(obj, prop, value) {
        if (obj[prop]) {
            if (Array.isArray(obj[prop])) {
                if (Array.isArray(value)) {
                    Array.prototype.push.apply(obj[prop], value)
                } else {
                    obj[prop].push(value);
                }
            } else {
                obj[prop] = [obj[prop], value];
            }
        } else {
            obj[prop] = value;
        }
    }

    // get value from object using dot notation
    function get_value(obj, prop, transforms, source) {
        if (prop === undefined) return {};
        if (prop === '') return obj;
        if (typeof obj === 'string') return obj;
        prop.split('.').every(function (part) {
            if (part === '[]' || part === '{}') {
                return false; // short circuit loop
            } else {
                return obj = (obj ? obj[part] : obj);
            }
        })
        if (transforms && transforms[0]) {
            return transforms[0](obj, source);
        } else {
            return obj;
        }
    }

    // convenience function for inline transforms
    $.mapper.transform = function (transform, def) {
        transform.type = 'transform';
        transform.def = def;
        return transform;
    }

    $.mapper.filter = function (filter, def) {
        var func = function (arr, source) {
            return arr.filter(filter, source)
        };
        func.type = 'filter';
        func.def = def;
        return func;
    }

    $.mapper.find = function (find, def) {
        var func = function (arr, source) {
            return arr.find(find, source)
        };
        func.type = 'transform';
        func.def = def;
        return func;
    }

    $.mapper.diff = function (orig, last, options) {
        options = options || {};
        if ($.type(orig) === "object" || $.type(orig) === "array") {
            var diff = $.type(orig) === "array" ? [] : {};
            Object.keys(orig).forEach(function (prop) {
                if (last.hasOwnProperty(prop)) {
                    var result = $.mapper.diff(orig[prop], last[prop], options);
                    if (result !== undefined && !($.type(result) === "array" && result.length === 0) && !($.type(result) === "object" && Object.keys(result).length === 0))
                        diff[prop] = result;
                } else {
                    if (!options.excludeOrig) diff[prop] = orig[prop];
                }
            });
            return diff;
        } else {
            if (orig !== last && (!options.ignore || options.ignore.find(function (item) { return item === last; }) === undefined) ) {
                return last;
            }
        }
    }
}(jQuery));
